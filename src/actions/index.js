import { MUSICAS_REQUEST, MUSICAS_SUCCESS, MUSICAS_FAILURE, MUSICAS_REFRESH_SUCCESS, MUSICAS_REFRESH_REQUEST, MUSICAS_UP_REQUEST, MUSICAS_UP_SUCCESS, MUSICAS_UP_FAILURE } from './actionTypes';

import { api } from '../services/api'

export function receberAlteracao(item) {
    return function action(dispatch) {
        dispatch({ type: MUSICAS_REFRESH_REQUEST })

        setTimeout(() => {
            dispatch({ type: MUSICAS_REFRESH_SUCCESS, item: item });
        }, 1500)
    }
};

export function alterarPosicao(item) {
    return function action(dispatch) {
        dispatch({ type: MUSICAS_UP_REQUEST })

        api.put('/musicas/' + item._id, item).then((res) => {
            dispatch({ type: MUSICAS_UP_SUCCESS, item: res.data });
        }).catch((error) => {
            dispatch({ type: MUSICAS_UP_FAILURE, error: error });
        })
    }
};

export function getMusicas() {
    return function action(dispatch) {
        dispatch({ type: MUSICAS_REQUEST })

        api.get('/musicas').then((res) => {
            dispatch({ type: MUSICAS_SUCCESS, registros: res.data.docs });
        }).catch((error) => {
            dispatch({ type: MUSICAS_FAILURE, error: error });
        })
    }
};
