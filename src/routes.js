import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Musicas from './pages/Musicas'

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Musicas} />
        </Switch>
    </BrowserRouter>
)

export default Routes