import { MUSICAS_REFRESH_REQUEST, MUSICAS_REFRESH_SUCCESS, MUSICAS_REQUEST, MUSICAS_SUCCESS, MUSICAS_FAILURE, MUSICAS_UP_REQUEST, MUSICAS_UP_SUCCESS, MUSICAS_UP_FAILURE } from '../actions/actionTypes';

const initialState = {
    registros: [],
    atualizando: false,
    item: {},
    loading: false,
    error: ''
};

export const musicasReducer = (state = initialState, action) => {
    switch (action.type) {
        case MUSICAS_REQUEST:
            return {
                ...state,
                loading: true,
                registros: [],
                error: ''
            };
        case MUSICAS_SUCCESS:
            return {
                ...state,
                loading: false,
                registros: ordenar(action.registros),
                error: ''
            };
        case MUSICAS_FAILURE:
            return {
                ...state,
                loading: false,
                registros: [],
                error: action.error
            };

        case MUSICAS_REFRESH_REQUEST:
            return {
                ...state,
                atualizando: true
            };
        case MUSICAS_REFRESH_SUCCESS:
            let registrosRefresh = [...state.registros.filter(regAtual => { return regAtual._id !== action.item._id }), action.item]

            return {
                ...state,
                registros: ordenar(registrosRefresh),
                atualizando: false
            };

        case MUSICAS_UP_REQUEST:
            return {
                ...state,
                atualizando: true
            };
        case MUSICAS_UP_SUCCESS:
            let registrosUp = [...state.registros.filter(regAtual => { return regAtual._id !== action.item._id }), action.item]

            return {
                ...state,
                registros: ordenar(registrosUp),
                atualizando: false
            };
        case MUSICAS_UP_FAILURE:
            return {
                ...state,
                item: {},
                atualizando: false
            };
        default:
            return state;
    }
};

const ordenar = (colDados) => {
    return colDados.sort(function (item1, item2) {
        return item2['score'] - item1['score']
    })
}