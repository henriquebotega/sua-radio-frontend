import React, { Component } from 'react'
import socket from 'socket.io-client'

import { Icon } from 'react-icons-kit'
import { ic_file_upload } from 'react-icons-kit/md/ic_file_upload'
import { ic_file_download } from 'react-icons-kit/md/ic_file_download'

import { api, ioURL } from '../../services/api'
import logo from '../../assets/logo.png'
import './styles.css'

import { bindActionCreators } from 'redux';
import { getMusicas, alterarPosicao, receberAlteracao } from '../../actions';
import { connect } from 'react-redux';


class Musicas extends Component {

    async componentDidMount() {
        this.subscribeToNewFiles();
        this.props.getMusicas();
    }

    subscribeToNewFiles = () => {
        const io = socket(ioURL)

        io.on('editMusica', data => {
            this.props.receberAlteracao(data);

            // let registros = [...this.props.musicas.registros.filter(item => { return item._id !== data._id }), data]
            // registros = this.ordenar(registros)
            // this.props.musicas.registros = registros;
        })
    }

    ordenar = (colDados) => {
        return colDados.sort(function (item1, item2) {
            return item2['score'] - item1['score']
        })
    }

    handleDiminuir = async (i) => {
        // Decrementa um ponto
        i.score = i.score - 1;
        this.props.alterarPosicao(i)
    }

    handleAumentar = async (i) => {
        // Incrementa um ponto
        i.score = i.score + 1;
        this.props.alterarPosicao(i)
    }

    render() {
        const { loading, atualizando, registros, error } = this.props.musicas;

        return (
            <div id="box-container">
                <header>
                    <img src={logo} alt="" />
                    <h1>Musicas</h1>
                </header>

                {atualizando && <div>Aguarde atualizando...</div>}

                {loading && <div>Aguarde carregando...</div>}

                {
                    !loading && error &&
                    <div>
                        <b>Error:</b> {JSON.stringify(error.response.status)} - {JSON.stringify(error.response.statusText)}
                    </div>
                }

                <ul>
                    {!loading && registros.length > 0 && registros.map((item, i) => (
                        <li key={i}>
                            <b>{item.score} - {item.titulo}</b>

                            <span>
                                <button onClick={() => this.handleDiminuir(item)}>
                                    <Icon size={24} icon={ic_file_download} />
                                </button>

                                <button onClick={() => this.handleAumentar(item)}>
                                    <Icon size={24} icon={ic_file_upload} />
                                </button>
                            </span>
                        </li>
                    ))}
                </ul>
            </div>
        )
    }
}

const mapStateToProps = state => ({ musicas: state.musicasReducer });

const mapDispatchToProps = dispatch => bindActionCreators({ alterarPosicao, receberAlteracao, getMusicas }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Musicas)